package com.jwt.dao;

import com.jwt.model.Organization;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author : schepuri
 * @version : v6.x
 * @since : 3/29/2018
 */
@Repository
public class OrganizationDAOImpl implements OrganizationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addOrganization(Organization organization) {
        sessionFactory.getCurrentSession().saveOrUpdate(organization);

    }

    @SuppressWarnings("unchecked")
    public List<Organization> getAllOrganizations() {

        return sessionFactory.getCurrentSession().createQuery("from Organization")
                .list();
    }

    @Override
    public void deleteOrganization(Integer organizationId) {
        Organization organization = (Organization) sessionFactory.getCurrentSession().load(
                Organization.class, organizationId);
        if (null != organization) {
            this.sessionFactory.getCurrentSession().delete(organization);
        }

    }

    public Organization getOrganization(int empid) {
        return (Organization) sessionFactory.getCurrentSession().get(
                Organization.class, empid);
    }

    @Override
    public Organization updateOrganization(Organization organization) {
        sessionFactory.getCurrentSession().update(organization);
        return organization;
    }

}