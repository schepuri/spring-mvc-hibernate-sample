package com.jwt.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Employee;
/**
 * @author : schepuri
 * @version : v6.x
 * @since : 3/29/2018
 */


@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addEmployee(Employee employee) {
        sessionFactory.getCurrentSession().saveOrUpdate(employee);

    }

    @SuppressWarnings("unchecked")
    public List<Employee> getAllEmployees() {

        //Using HQL Query
        /*return sessionFactory.getCurrentSession().createQuery("from Employee")
                .list();*/
        //Using Criteria
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);
        criteria.setMaxResults(1)
                .add( Restrictions.like("name", "s%"))
                .addOrder( Order.desc("name") );
        return criteria.list();
    }

    @Override
    public void deleteEmployee(Integer employeeId) {
        Employee employee = (Employee) sessionFactory.getCurrentSession().load(
                Employee.class, employeeId);
        if (null != employee) {
            this.sessionFactory.getCurrentSession().delete(employee);
        }

    }

    public Employee getEmployee(int empid) {
        return (Employee) sessionFactory.getCurrentSession().get(
                Employee.class, empid);
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        sessionFactory.getCurrentSession().update(employee);
        return employee;
    }

}