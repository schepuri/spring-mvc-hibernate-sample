package com.jwt.dao;

import com.jwt.model.Organization;

import java.util.List;

/**
 * @author : schepuri
 * @version : v6.x
 * @since : 3/29/2018
 */
public interface OrganizationDAO {

    public void addOrganization(Organization organization);

    public List<Organization> getAllOrganizations();

    public void deleteOrganization(Integer organizationId);

    public Organization updateOrganization(Organization organization);

    public Organization getOrganization(int organizationid);
}
