package com.jwt.service;

import com.jwt.dao.EmployeeDAO;
import com.jwt.dao.OrganizationDAO;
import com.jwt.model.Employee;
import com.jwt.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationDAO organizationDAO;

    @Override
    @Transactional
    public void addOrganization(Organization organization) {
        organizationDAO.addOrganization(organization);
    }

    @Override
    @Transactional
    public List<Organization> getAllOrganizations() {
        return organizationDAO.getAllOrganizations();
    }

    @Override
    @Transactional
    public void deleteOrganization(Integer organizationId) {
        organizationDAO.deleteOrganization(organizationId);
    }

    public Organization getOrganization(int empid) {
        return organizationDAO.getOrganization(empid);
    }

    public Organization updateOrganization(Organization organization) {
        // TODO Auto-generated method stub
        return organizationDAO.updateOrganization(organization);
    }

    public void setOrganizationDAO(OrganizationDAO organizationDAO) {
        this.organizationDAO = organizationDAO;
    }

}