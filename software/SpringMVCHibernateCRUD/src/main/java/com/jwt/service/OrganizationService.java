package com.jwt.service;

import com.jwt.model.Employee;
import com.jwt.model.Organization;

import java.util.List;

public interface OrganizationService {

    public void addOrganization(Organization organization);

    public List<Organization> getAllOrganizations();

    public void deleteOrganization(Integer organizationId);

    public Organization getOrganization(int organizationid);

    public Organization updateOrganization(Organization organization);
}